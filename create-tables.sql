

CREATE table memos(
    id SERIAL primary key,
    content TEXT,
    image varchar(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);


CREATE table users(
    id SERIAL primary key,
    username varchar(255),
    password varchar(255),
    created_at timestamp,
    updated_at timestamp
);