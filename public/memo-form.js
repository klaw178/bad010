import { loadMemos } from "./lib.js";

document
    .querySelector("#memo-form")
    .addEventListener("submit", async function (event) {
        // 截咗原本個submit
        event.preventDefault();
        // 攞返個form 嘅element 出嚟
        const form = event.target;

        const formData = new FormData();
        formData.append("content", form.content.value);
        if (form.image.files[0]) {
            formData.append("image", form.image.files[0]);
        }

        const res = await fetch("/memos", {
            method: "POST",
            body: formData, //formData唔需要Content-Type 果個header
        });

        if (res.status === 200) {
            console.log(await res.json());
        }
        form.reset();
        loadMemos();
    });
