// to store current user info
let currentUser = null;

export async function getCurrentUser() {
    const res = await fetch("/current-user");
    const result = await res.json();

    if (res.status === 200) {
        currentUser = result;
        document.querySelector(
            "#login-section"
        ).innerHTML = `<h3>歡迎!${currentUser.username}</h3>`;
    }
}

export async function loadMemos() {
    const res = await fetch("/memos");
    const memos = await res.json();
    console.log(memos);

    const memoContainer = document.querySelector(".memo-container");
    memoContainer.innerHTML = "";
    for (let memo of memos) {
        memoContainer.innerHTML += /*html*/ `
        <div class="memo" contenteditable > 
            ${memo.content}
            ${
                memo.image
                    ? `<img src="/uploads/${memo.image}" class="img-fluid" />`
                    : ""
            }
            ${
                currentUser
                    ? `
                <div class="like-button" data-id="${memo.id}">
                    <i class="fas fa-thumbs-up"></i>
                </div>
                <div class="trash-button" data-id="${memo.id}">
                    <i class="fas fa-trash"></i>
                </div>
                <div class="edit-button" data-id="${memo.id}">
                    <i class="fas fa-edit"></i>
                </div>
                `
                    : ""
            }
           
        </div>
        `;
    }

    // 1. addeventlistener to 所有嘅edit-button 嘅
    const editButtons = document.querySelectorAll(".edit-button");
    for (let editButton of editButtons) {
        editButton.addEventListener("click", async function (event) {
            // data-index呢個attribute任你去做，叫咩名都得
            const id = event.currentTarget.getAttribute("data-id");
            const content = event.currentTarget.closest(".memo").innerText; // closest 符合條件的祖宗
            const res = await fetch(`/memos/${id}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    content: content,
                }),
            });
            const result = await res.json();
            if (res.status == 200 && result.success) {
                loadMemos(); //  不一定要係呢個位再load ，不過再load 就易做啲
            } else {
                alert("Update failed " + result.message);
            }
        });
    }
    const deleteButtons = document.querySelectorAll(".trash-button");
    for (let deleteButton of deleteButtons) {
        deleteButton.addEventListener("click", async function (event) {
            const id = event.currentTarget.getAttribute("data-id");
            const res = await fetch(`/memos/${id}`, {
                method: "DELETE",
            });
            const result = await res.json();
            if (res.status == 200 && result.success) {
                loadMemos();
            } else {
                alert("Delete failed " + result.message);
            }
        });
    }
    // 2. 當有人撳粒button時， 用event.currentTarget.getAttribute('data-index')攞返呢粒制對應的index
    // 3. 將個index 當係params 射上server

    const likeButtons = document.querySelectorAll(".like-button");
    for (let likeButton of likeButtons) {
        likeButton.addEventListener("click", async function (event) {
            const id = event.currentTarget.getAttribute("data-id");
            const res = await fetch(`/like/memo-id/${id}`, {
                method: "POST",
            });
            const result = await res.json();
            if (res.status == 200 && result.success) {
                loadMemos();
            } else {
                alert("Like failed " + result.message);
            }
        });
    }
}
