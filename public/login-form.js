import { socket, initSocket } from "./index.js";
import { getCurrentUser } from "./lib.js";

document
    .querySelector("#login-form")
    .addEventListener("submit", async function (event) {
        // 截咗原本個submit
        event.preventDefault();
        // 攞返個form 嘅element 出嚟
        const form = event.target;
        const loginInfo = {};
        loginInfo.username = form.username.value;
        loginInfo.password = form.password.value;

        login(loginInfo);
        form.reset();
    });

export async function login(loginInfo) {
    const res = await fetch("/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(loginInfo),
    });

    if (res.status === 200) {
        console.log(await res.json());
        // 成功的話，改個location
        getCurrentUser();
        // 因為如果你一開頭未login ，咁socket.io 係access 唔到session
        // 如果你成頁refresh 咗都得
        // location.reload();
        socket.disconnect();
        initSocket();
    } else {
        const result = await res.json();
        const loginForm = document.querySelector("#login-form");
        loginForm.innerHTML += `
            <div class="alert alert-danger" role="alert">
                ${result.message}
            </div>
        `;
    }
}
