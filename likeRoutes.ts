import express from "express";
import { isLoggedInAPI } from "./guard";
import { likeController } from "./main";

export const likeRoutes = express.Router();

likeRoutes.get("/like-memos", isLoggedInAPI, likeController.getLikeMemos);
likeRoutes.post("/like/memo-id/:id", isLoggedInAPI, likeController.insertLike);
