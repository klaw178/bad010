import express from "express";
import { logger } from "../logger";
import { LikeService } from "../services/LikeService";


export class LikeController {
    constructor(private likeService: LikeService) {

    }

    getLikeMemos = async (req: express.Request, res: express.Response) => {
        const userId = parseInt(req.query["user_id"] + "");
        if (isNaN(userId)) {
            res.status(400).json({ message: "user id is not an integer" });
            return;
        }
        const result = await this.likeService.getLikeMemos(userId)
        res.json(result);
    }

    insertLike = async (req: express.Request, res: express.Response) => {
        try {
            const memoId = parseInt(req.params.id); //<-- 必然係string
            if (isNaN(memoId)) {
                res.status(400).json({ message: "memo id  is not an integer" });
                return;
            }
            await this.likeService.insertLike(req.session["user"].id, memoId);
            logger.info(`Successfully like the memo of id ${memoId}`);
            res.json({ success: true });
        } catch (e) {
            // 一係將error 畀前面
            // 一係log 低佢
            logger.error(e);
            res.status(500).json({ message: "Cannot like memo" });
        }
    }
}