import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    const users = await knex.select('*').from('users').where('username', 'gordon@tecky.io');
    if (users.length === 0) {
        await knex.insert({
            username: 'gordon@tecky.io',
            password: '$2a$10$OYV.xFOzyryYG27zMNutB.q2O2I2RSTvNNHona6Uq3E6NZNyqXN0q'
        }).into('users')
    }
};
