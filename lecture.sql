



ALTER TABLE memos add column json jsonb ;

select * from memos where content = 'testing';
select * from users where username = 'gordon@tecky.io';

insert into memos (content,image,created_at,updated_at,json)
values ('testing',null,NOW(),NOW(),'{"my-key":"my-value"}');


CREATE TABLE likes (
    id SERIAL PRIMARY KEY,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    memo_id integer,
    FOREIGN KEY (memo_id) REFERENCES memos(id)
);

insert into likes (user_id,memo_id) values (46,525);

select memos.* from users
        INNER JOIN likes on users.id = likes.user_id
        INNER JOIN memos on memos.id = likes.memo_id
        WHERE users.id = 46;