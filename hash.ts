import bcrypt from "bcryptjs";

const SALT_ROUNDS = 10;

// Plaintext hash 做 hashed string
export async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

// 另一段hash 咗佢，再同hashed string 比較
export async function checkPassword(
    plainPassword: string,
    hashPassword: string
) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
}

hashPassword("tecky").then(console.log);

// hashPassword('tecky')
//     .then(hash=>hash === '$2a$10$1KTl78kZBbtqXmmqttOfce0KnpCDus9eRyZWxBY9iftL77u9J./p6' )
//     .then(console.log)

// checkPassword('tecky','$2a$10$1KTl78kZBbtqXmmqttOfce0KnpCDus9eRyZWxBY9iftL77u9J./p6')
//     .then(console.log)
//     .catch(console.log);
