async function loadLikedMemos() {
    const search = new URLSearchParams(location.search);
    const userId = search.get("user_id");
    const res = await fetch("/like-memos?user_id=" + userId);
    const result = await res.json();

    document.querySelector("#liked-memos").innerHTML = JSON.stringify(
        result,
        null,
        { spaces: 4 }
    );
}

loadLikedMemos();
