import express from "express";
import { isLoggedInAPI } from "./guard";
import { memoController, upload } from "./main";

export const memoRoutes = express.Router();

memoRoutes.post("/memos", upload.single("image"), memoController.postMemos);
memoRoutes.get("/memos", memoController.getMemos);
memoRoutes.delete("/memos/:id", isLoggedInAPI, memoController.deleteMemo);
memoRoutes.put("/memos/:id", isLoggedInAPI, memoController.updateMemo);


