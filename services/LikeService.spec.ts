import Knex from 'knex';
const knexfile = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.
import { LikeService } from './LikeService';
import { Memo } from './models';

describe("MemoService", () => {

    let likeService: LikeService;
    let memos: Memo[];
    let memo: any;
    beforeEach(async () => {
        memos = [
            {
                content: "Test1 for LikeService",
                image: "456.png"
            },
            {
                content: "Test2 for LikeService",
                image: "123.png"
            }
        ]
        await knex.insert(memos).into('memos');
        memo = await knex.select('id').from('memos').where('content', 'Test1 for LikeService');
        await knex.insert({
            user_id: 1,
            memo_id: memo[0].id
        }).into('likes');
        likeService = new LikeService(knex);
    })

    it("should get liked memos", async () => {
        let fake = {
            content: 'Test1 for LikeService',
            image: '456.png'
        }

        const memosReturned = await likeService.getLikeMemos(1)
        expect(memosReturned[0]).toMatchObject(fake);
    });

    it("should insert like", async () => {
        let fake = {
            content: 'Test3 for LikeService',
        }

        await knex.insert(fake).into('memos');
        memo = await knex.select('id').from('memos').where('content', 'Test3 for LikeService');
        await likeService.insertLike(1, memo[0].id)
        const memosReturned = await likeService.getLikeMemos(1)
        expect(memosReturned[1]).toMatchObject(fake);
    });

    afterEach(async () => {
        await knex('likes').del();
        await knex('memos').where('content', 'Test1 for LikeService').orWhere('content', 'Test2 for LikeService').orWhere('content', 'Test3 for LikeService').del();
    });

    afterAll(() => {
        knex.destroy();
    });
})