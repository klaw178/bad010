import { Knex } from 'knex';
import { Memo } from './models';

export class MemoService {

    // 將個Database client 由constructor 塞入呢個Class 入面
    constructor(private knex: Knex) {
        // Dependency injection
        // 1. 可以換唔同嘅Database (Testing Database)
        // 2. 甚至乎可以放一個mock 入去
    }

    async postMemosWithFile(content: string, fileName: string): Promise<{ id: number }[]> {
        return await this.knex.insert({
            content: content,
            image: fileName,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('memos').returning('id');
    }

    async postMemosWithoutFile(content: string): Promise<{ id: number }[]> {
        return await this.knex.insert({
            content: content,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('memos').returning('id');
    }

    async listMemo(): Promise<Memo[]> {
        const memos = await this.knex.select('*').from('memos').orderBy('updated_at', 'desc').limit(10);
        return memos;
    }

    async searchMemos(q: string): Promise<Memo[]> {
        const filteredMemos = await this.knex.select('*').from('memos').where('content', 'like', `%${q}%`).orderBy('updated_at', 'desc').limit(10);
        return filteredMemos;
    }

    async deleteMemo(id: number) {
        return await this.knex('memos').where('id', id).del();
    }

    async updateMemo(content: any, id: number) {
        return await this.knex('memos').update({
            content: content,
            updated_at: new Date()
        }).where('id', id);
    }
}