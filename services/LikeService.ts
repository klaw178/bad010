import { Knex } from 'knex';

export class LikeService {

    constructor(private knex: Knex) {
        // Dependency injection
        // 1. 可以換唔同嘅Database (Testing Database)
        // 2. 甚至乎可以放一個mock 入去
    }

    async getLikeMemos(userId: Number) {
        return await this.knex.select('memos.*').from('users').innerJoin('likes', 'users.id', 'likes.user_id').innerJoin('memos', 'memos.id', 'likes.memo_id').where('users.id', userId);
    }

    async insertLike(userId: Number, memoId: Number) {
        return await this.knex.insert({
            user_id: userId,
            memo_id: memoId,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('likes');

    }

}