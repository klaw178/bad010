import Knex from 'knex';
const knexfile = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.
import { MemoService } from './MemoService';
import { Memo } from './models';

describe("MemoService", () => {

    let memoService: MemoService;
    let memos: Memo[];

    beforeEach(async () => {
        memos = [
            {
                content: "I am a test",
                image: "456.png"
            },
            {
                content: "I am test 2",
                image: "123.png"
            }
        ]
        await knex.insert(memos).into('memos');
        memoService = new MemoService(knex);
    })

    it("should create memos with file", async () => {
        let fake = {
            content: 'hihi',
            image: '789.jpg'
        }

        await memoService.postMemosWithFile('hihi', '789.jpg')
        const memosReturned = await memoService.listMemo();
        expect(memosReturned[0]).toMatchObject(fake);
    });

    it("should create memos without file", async () => {
        let fake = {
            content: 'hihi',
        }

        await memoService.postMemosWithoutFile('hihi')
        const memosReturned = await memoService.listMemo();
        expect(memosReturned[0]).toMatchObject(fake);
    });

    it("should get all memos", async () => {
        const memosReturned = await memoService.listMemo();
        expect(memosReturned).toMatchObject(memos);
    });

    it("should search memos", async () => {
        const memosFiltered = await memoService.searchMemos("I am a test");
        expect(memosFiltered).toMatchObject([memos[0]]);
    });

    it("should delete memos", async () => {
        const memosOrigined: Memo[] = await memoService.listMemo();
        await memoService.deleteMemo(memosOrigined[0].id as number);
        const memosReturned = await memoService.listMemo();
        expect(memosReturned).toMatchObject([memos[1]]);
    });

    it("should update memos", async () => {
        let fake = {
            content: 'hihi',
        }

        const memosOrigined: Memo[] = await memoService.listMemo();
        await memoService.updateMemo('hihi', memosOrigined[0].id as number);
        const memosReturned = await memoService.listMemo();
        expect(memosReturned[0]).toMatchObject(fake);
    });

    afterEach(async () => {
        await knex('memos').where('content', 'I am a test').orWhere('content', 'I am test 2').orWhere('content', 'hihi').del();
    });

    afterAll(() => {
        knex.destroy();
    });
})